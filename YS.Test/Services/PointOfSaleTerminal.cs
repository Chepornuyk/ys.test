﻿using System.Collections.Generic;
using YS.Test.Data.Models;
using YS.Test.Data.Models.Response;

namespace YS.Test.Services
{
    public class PointOfSaleTerminal : IPointOfSaleTerminal
    {
        public double CalculateTotal(IList<Goods> goods)
        {
            double result = 0;

            foreach (var item in goods)
            {
                if (item.Product.IsValuePrice)
                    result += CalculateValuePrice(item);
                else
                    result += item.Amount * item.Product.Price;
            }

            return result;
        }

        public PriceWithDiscountResponse CalculateTotalWithDiscount(IList<Goods> goods, Discount discount)
        {
            double price = 0;
            double priceForDiscount = 0;

            foreach (var item in goods)
            {
                if (item.Product.IsValuePrice)
                    price += CalculateValuePrice(item);
                else
                {
                    price += item.Amount * item.Product.Price;
                    priceForDiscount += item.Amount * item.Product.Price;
                }
            }
            discount.AddAmount(priceForDiscount);

            return MakeResult(price, discount);
        }

        private PriceWithDiscountResponse MakeResult(double price, Discount discount) =>
            new PriceWithDiscountResponse
            {
                Price = $"Price {price}",
                Discount = discount.ToString(),
            };


        private double CalculateValuePrice(Goods goods)
        {
            double result = 0;

            if (goods.Amount >= goods.Product.ValueAmount)
            {
                var productsWithClearPrice = goods.Amount % goods.Product.ValueAmount;
                result += productsWithClearPrice * goods.Product.Price;

                var productsWithValuePrice = (goods.Amount - productsWithClearPrice) / goods.Product.ValueAmount;
                result += productsWithValuePrice * goods.Product.ValuePrice;
            }
            else
                result += goods.Amount * goods.Product.Price;


            return result;
        }
    }
}
