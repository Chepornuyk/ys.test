﻿using System.Collections.Generic;
using YS.Test.Data.Models;
using YS.Test.Data.Models.Response;

namespace YS.Test.Services
{
    public interface IPointOfSaleTerminal
    {
        double CalculateTotal(IList<Goods> goods);

        PriceWithDiscountResponse CalculateTotalWithDiscount(IList<Goods> goods, Discount discount);
    }
}
