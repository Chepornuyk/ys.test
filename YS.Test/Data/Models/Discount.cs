﻿namespace YS.Test.Data.Models
{
    public class Discount
    {
        public double Amount { get; private set; }

        public Discount(double amount)
        {
            Amount = amount;
        }

        public void AddAmount(double amount) =>
            Amount += amount * GetDiscountPercent(amount);

        public void SubtractAmount(double amount) =>
            Amount -= amount;

        public override string ToString() =>
             $"Discount amount: {Amount}";



        private static double GetDiscountPercent(double amount)
        {
            double result = default;
            if (amount >= 1000 && amount < 2000)
                result = 0.01;
            else if (amount >= 2000 && amount < 5000)
                result = 0.03;
            else if (amount >= 5000 & amount < 10000)
                result = 0.05;
            else if (amount > 9999)
                result = 0.07;

            return result;
        }
    }
}
