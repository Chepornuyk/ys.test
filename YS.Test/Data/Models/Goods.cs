﻿namespace YS.Test.Data.Models
{
    public struct Goods
    {
        public Product Product { get; set; }

        public int Amount { get; set; }
    }
}
