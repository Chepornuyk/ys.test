﻿using System.Collections.Generic;

namespace YS.Test.Data.Models.Request
{
    public struct PayWithDiscountRequest
    {
        public IList<string> ProductNames { get; set; }

        public double Discount { get; set; }
    }
}
