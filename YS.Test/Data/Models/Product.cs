﻿namespace YS.Test.Data.Models
{
    public class Product
    {
        public string Name { get; set; }

        public double Price { get; set; }

        public bool IsValuePrice { get; set; }

        public double ValuePrice { get; set; }

        public int ValueAmount { get; set; }
    }
}
