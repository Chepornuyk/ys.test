﻿namespace YS.Test.Data.Models.Response
{
    public struct PriceWithDiscountResponse
    {
        public string Price { get; set; }

        public string Discount { get; set; }
    }
}
