﻿using System.Collections;
using System.Collections.Generic;
using YS.Test.Data.Models;

namespace YS.Test.Data
{
    internal static class ResourceData
    {
        public static IList<Product> Products = new List<Product>()
        {
            new Product
            {
                Name = "A",
                Price = 1.25,
                IsValuePrice = true,
                ValuePrice = 3,
                ValueAmount = 3
            },
            new Product
            {
                Name = "B",
                Price = 4.25,
                IsValuePrice = false,
            },
            new Product
            {
                Name = "C",
                Price = 1,
                IsValuePrice = true,
                ValuePrice = 5,
                ValueAmount = 6,
            },
            new Product
            {
                Name = "D",
                Price = 0.75,
                IsValuePrice = false,
            }
        };
    }
}
