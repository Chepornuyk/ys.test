﻿using System.Collections.Generic;
using YS.Test.Data.Models;

namespace YS.Test.Data.DataProvider
{
    /// <summary>
    /// Dataprovider for load some data from any source and map it for response
    /// </summary>
    public interface IGoodsDataProvider
    {
        /// <summary>
        /// Group product name and count it, load product info
        /// </summary>
        /// <param name="productNames">List of product names</param>
        /// <returns>Product info and amout</returns>
        IList<Goods> GetGoodsInfo(IEnumerable<string> productNames);
    }
}
