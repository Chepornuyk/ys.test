﻿using System.Collections.Generic;
using System.Linq;
using YS.Test.Data.Models;
using YS.Test.Errors;

namespace YS.Test.Data.DataProvider
{
    public class GoodsDataProvider : IGoodsDataProvider
    {
        public IList<Goods> GetGoodsInfo(IEnumerable<string> productNames)
        {
            var grouped = productNames.GroupBy(d => d)
                .Select(d => new { Name = d.Key, Amount = d.Count() })
                .ToList();

            return grouped.Select(d => MakeGoods(d.Name, d.Amount)).ToList();
        }

        private Goods MakeGoods(string productName, int amount)
        {
            var product = ResourceData.Products
                        .Where(d => d.Name == productName)
                        .FirstOrDefault();

            if (product == null)
                throw new NotExistsProductException($"Product {productName} not found");

            return new Goods { Product = product, Amount = amount };
        }
    }
}
