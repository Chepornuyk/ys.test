﻿using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc.Controllers;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using YS.Test.Swwager;
using YS.Test.Data.DataProvider;
using YS.Test.Services;

namespace YS.Test
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                    .AddXmlDataContractSerializerFormatters()
                    .AddJsonOptions(x => { x.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Include; });

            services.AddMvcCore().AddDataAnnotations();
            services.AddApiVersioning();

            ConfigureTransient(services);

            var serviceProvider = services.BuildServiceProvider();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "YouScan API", Version = "v1" });


                c.DocInclusionPredicate((docName, apiDesc) =>
                {
                    var controllerActionDescriptor = apiDesc.ActionDescriptor as ControllerActionDescriptor;
                    var attributes = controllerActionDescriptor?.GetControllerAndActionAttributes(true)
                                     ?? Enumerable.Empty<object>();

                    var versions = attributes.OfType<ApiVersionAttribute>().SelectMany(attr => attr.Versions);

                    return versions.Any(v => $"v{v.ToString()}" == docName);
                });

                c.OperationFilter<RemoveVersionParameters>();
                c.DocumentFilter<SetVersionInPaths>();
            });
        }

        private void ConfigureTransient(IServiceCollection services)
        {
            services.AddTransient<IGoodsDataProvider, GoodsDataProvider>();

            services.AddTransient<IPointOfSaleTerminal, PointOfSaleTerminal>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "YouScan API v1"); });
            app.UseMvc();
        }
    }
}
