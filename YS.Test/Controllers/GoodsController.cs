﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using YS.Test.Data.DataProvider;
using YS.Test.Data.Models;
using YS.Test.Data.Models.Request;
using YS.Test.Errors;
using YS.Test.Services;

namespace YS.Test.Controllers
{
    [ApiVersion("1")]
    [Route("v{version:apiVersion}/[controller]/[action]")]
    public class GoodsController : Controller
    {
        private readonly IGoodsDataProvider _goodsDataProvider;
        private readonly IPointOfSaleTerminal _terminal;

        public GoodsController(IGoodsDataProvider goodsDataProvider, IPointOfSaleTerminal terminal)
        {
            _goodsDataProvider = goodsDataProvider ?? throw new ArgumentNullException(nameof(goodsDataProvider));
            _terminal = terminal ?? throw new ArgumentNullException(nameof(terminal));
        }

        [HttpPost]
        public IActionResult Pay([FromBody]  IList<string> productNames)
        {
            if (!productNames.Any())
                return BadRequest();

            productNames = productNames.Where(s => !string.IsNullOrEmpty(s)).ToList();

            IList<Goods> goods;
            try
            {
                goods = _goodsDataProvider.GetGoodsInfo(productNames);
            }
            catch (NotExistsProductException ex)
            {
                return NotFound(ex.Message);
            }

            var result = _terminal.CalculateTotal(goods);

            return Ok(result);
        }

        [HttpPost]
        public IActionResult PayWithDiscount([FromBody] PayWithDiscountRequest data)
        {
            if (!data.ProductNames.Any() || data.Discount < 0)
                return BadRequest();

            data.ProductNames = data.ProductNames.Where(s => !string.IsNullOrEmpty(s)).ToList();

            IList<Goods> goods;
            try
            {
                goods = _goodsDataProvider.GetGoodsInfo(data.ProductNames);
            }
            catch (NotExistsProductException ex)
            {
                return NotFound(ex.Message);
            }

            var discount = new Discount(data.Discount);

            var result = _terminal.CalculateTotalWithDiscount(goods, discount);

            return Ok(result);
        }
    }

}
