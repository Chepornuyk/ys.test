﻿using System;

namespace YS.Test.Errors
{
    [Serializable]
    public class NotExistsProductException : Exception
    {
        public NotExistsProductException() { }

        public NotExistsProductException(string message) : base(message) { }

        public NotExistsProductException(string message, Exception innerException) : base(message, innerException) { }
    }
}
