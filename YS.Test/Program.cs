﻿using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace YS.Test
{
    public class Program
    {
        private static IConfiguration StartupConfiguration { get; set; }

        private static IPAddress ServerHost
        {
            get
            {
                return IPAddress.Parse(StartupConfiguration["Startup:ServerHost"]);
            }
        }

        private static int ServerPort
        {
            get
            {
                return int.Parse(StartupConfiguration["Startup:ServerPort"]);
            }
        }

        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Run();
        }

        public static IWebHost CreateWebHostBuilder(string[] args)
        {
            InitStartupConfiguration(args);

            return WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseKestrel(options =>
                {
                    options.Listen(ServerHost, ServerPort);
                })
                .Build();
        }

        private static void InitStartupConfiguration(string[] args)
        {
            var parameters = new Dictionary<string, string>
            {
                {"--host", "Startup:ServerHost"},
                {"--port", "Startup:ServerPort"},
            };

            var builder = new ConfigurationBuilder();
            builder.AddCommandLine(args, parameters);

            StartupConfiguration = builder.Build();
        }
    }
}
