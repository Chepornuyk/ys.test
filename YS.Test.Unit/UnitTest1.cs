using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using YS.Test.Controllers;
using YS.Test.Data.DataProvider;
using YS.Test.Services;

namespace YS.Test.Unit
{
    public class UnitTest1
    {
        private static List<string> testData1 = new List<string> { "A", "B", "C", "D", "A", "B", "A" };
        private static List<string> testData2 = new List<string> { "C", "C", "C", "C", "C", "C", "C" };
        private static List<string> testData3 = new List<string> { "A", "B", "C", "D" };

        [Fact]
        public void Test1()
        {
            var terminal = new PointOfSaleTerminal();
            var dataprovider = new GoodsDataProvider();
            var controller = new GoodsController(dataprovider, terminal);

            var result1 = controller.Pay(testData1);
            var okResult1 = Assert.IsType<OkObjectResult>(result1);

            var result2 = controller.Pay(testData2);
            var okResult2 = Assert.IsType<OkObjectResult>(result2);

            var result3 = controller.Pay(testData3);
            var okResult3 = Assert.IsType<OkObjectResult>(result3);

            Assert.Equal("13.25", okResult1.Value.ToString());
            Assert.Equal("6", okResult2.Value.ToString());
            Assert.Equal("7.25", okResult3.Value.ToString());
        }
    }
}
